+++
title = "About"
date = "2020-08-08"
aliases = ["about","about","contact"]
[ author ]
  name = "accraze"
+++

<img class="wp-image-107 aligncenter" alt="IMG_0451" src="http://res.cloudinary.com/accraze/image/upload/v1412744568/IMG_04511_ehoog0.jpg" width ="50%" align="center"/>
<div><center>

<a href="https://github.com/accraze" target="_blank">Github</a>
<a href="http://www.twitter.com/accraze" target="_blank">Twitter</a> </center></div>


I am a forest-dwelling human being living on the Oregon Coast.

My interest range from music, nature, artificial intelligence and open source computing.

☮ Contact Info ☮

* GPG: EEF6 0A69 6305 999B

* IRC: accraze (libera.chat)

gemini://accraze.space/

