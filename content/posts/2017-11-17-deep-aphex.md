---
title: Deep Learning Aphex Twin
date: 2017-11-17
---
Can machines create art or music? This is the question that Google Brain's Magenta project has been actively exploring. Over the last couple of weeks I've been playing with various models provided by Magenta to see what kind of musical possibilities Neural Nets have to offer.

Given my background with music, I wanted to see what sounds I could create when training my own Performance RNN model with a bunch of Aphex Twin MIDI files. I then took all of the generated midi files together and tried to randomly construct them into a "song". Here is a link to the song on soundcloud:
<iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/354558851&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true"></iframe>

I created my own dataset with 39 MIDI files that I found at http://www.midm-database.co.uk/AFX.html and then trained a Performance RNN model while tweaking various hyperparameters in order to get the most interesting sounds.

The songs in the dataset ranged from meditative ambient music to chaotically dense techno with alien rhythms and dynamics. When I was training the model, I saw a bunch of warnings about Key signatures and Time signatures, which makes sense due to how broadly the dynamics ranged between each song. In the end, the model I used to generate my MIDI files had a high loss function (~ 5.5 - 6), but the dynamics looked much more interesting.
<p><img src="https://raw.githubusercontent.com/accraze/afx_rnn/master/imgs/loss.png" width="279" height="184" style="width: 279px; height: 184px;"><img src="https://raw.githubusercontent.com/accraze/afx_rnn/master/imgs/perplexity.png" width="269" height="184" style="width: 269px; height: 184px;"></p>

When I generated the AFX-RNN MIDI files, I primed the model with a midi file of the middle section of the song "Windowlicker". I chose this particular midi files since the beat is fairly straight forward and there are simple polyphonic sequences.

Once the new midi files were generated, I imported them into Ableton Live and scattered all 10 files randomly. I then placed various synths and an 808 Drum patch randomly onto the MIDI files and also removed the priming sequence from the beginning of each of the files.

<p><img src="https://raw.githubusercontent.com/accraze/afx_rnn/master/imgs/afx-abl.jpg"></p>

I then messed with the levels and attempted to make an AI generated Aphex Twin track with all 10 generated MIDI files from the Performance RNN model.

Here is a quick video demo of the process:
<iframe src="https://player.vimeo.com/video/242427526" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/242427526">Deep Learning Aphex Twin</a> from <a href="https://vimeo.com/accraze">accraze</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


Code: https://github.com/accraze/afx_rnn
